<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Material;
use DB;
use Auth;
use App\User;
use App\MaterialLogs;
use Mail;


class MaterialController extends Controller
{
    public function listMaterial(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;


        $rules = array(
            'user_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $id = $request->user_id;
            $users = User::where('users.id', $id)->first();

            if($users){
                // $listMaterial = Material::join('jobassignemployee','job.id','=','jobassignemployee.job_id')
                // ->where('jobassignemployee.job_employee_id',$id)
                // ->where('job.job_status','!=','markasdone')
                // ->select('job.id as relation_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id');
                $listMaterial = Material::select('*','material.id as material_id');

                if ($request->search != ''){
                    $search = $request->search;
                    $where_filter = "(material_name LIKE  '%$search%')";
                    $listMaterial = $listMaterial->whereRaw($where_filter);

                }
                $listMaterial  = $listMaterial->paginate(10);

                if(count($listMaterial)>0){
                    $data=$listMaterial;
                }
                else{
                    $status = false;
                    $message = 'No Material Found';
                    $code = 400;
                }
            }
            else{
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }
        }

        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }


    public function listMaterialWithoutPagination(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;


        $rules = array(
            'user_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $id = $request->user_id;
            $users = User::where('users.id', $id)->first();

            if($users){

                $listMaterial = Material::select('*','material.id as material_id');

                if ($request->search != ''){
                    $search = $request->search;
                    $where_filter = "(material_name LIKE  '%$search%')";
                    $listMaterial = $listMaterial->whereRaw($where_filter);

                }
                $listMaterial  = $listMaterial->get();

                if(count($listMaterial)>0){
                    $data=$listMaterial;
                }
                else{
                    $status = false;
                    $message = 'No Material Found';
                    $code = 400;
                }
            }
            else{
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }
        }

        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }


    public function materialDetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'material_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $id = $request->user_id;
            $material_id = $request->material_id;
            $users= User::where('users.id',$id)->first();

            if($users){
                $materialdata = Material::where('id',$material_id)
                ->select('*','material.id as material_id')
                ->first();
                if($materialdata != null){
                    $data=$materialdata;
                    $message = 'Success';
                }
                else{
                    $status = false;
                    $message = 'No material Found';
                    $code = 400;
                }
            }
            else{
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }
        }

        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }

    public function getMaterialUsed(Request $request){
        $data = 0;
        $message = "";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'material_id'=>'required|integer',
            'quantity'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $id = $request->user_id;
            $material_id = $request->material_id;
            $quantity = $request->quantity;
            $description = $request->description;
            $users= User::where('users.id',$id)->first();

            if($users){
                $materialdata = Material::where('id',$material_id)
                ->select('*','material.id as material_id')
                ->first();
                if($materialdata != null){
                    if($quantity >= $materialdata->stock)
                    {
                        $status = false;
                        $message = 'Out of stock for '.$materialdata->material_name;
                        $code = 400;
                    }
                    else {
                        $value = $materialdata->stock - $quantity;

                        if ($value < $materialdata->min_stock) {
                            $status = false;
                            $message = $materialdata->stock . ' Material are in stock.so please enter as per stock quantity.';
                            $code = 400;
                        }
                        else {
                            $requestData['stock'] = $value;
                            $materialdata->update($requestData);

                            if ($value == $materialdata->min_stock) {
                                $admin = User::where('users.id', '1')->first();

                                Mail::send('admin.emails.stockmail', compact('admin', 'value','materialdata'), function ($message) use ($admin, $value,$materialdata) {
                                    $message->to($admin->email)->subject('Stock Remainder');
                                });
                            }
                            $data = $value;
                            $materialLogsData['material_id'] = $materialdata->id;
                            $materialLogsData['user_id'] = $id;
                            $materialLogsData['quantity'] = $quantity;
                            $materialLogsData['description'] = ((isset($description) && $description != null ) ? $description : '');
                            $materialLogsData['action'] = 1;
                            $materialLogs = MaterialLogs::create($materialLogsData);

                            $message = 'Success';
                        }
                    }

                }
                else{
                    $status = false;
                    $message = 'No material Found';
                    $code = 400;
                }
            }
            else{
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }
        }
        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

}
