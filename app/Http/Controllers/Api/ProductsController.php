<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use App\Productinquiry;
use App\ProductLogs;

class ProductsController extends Controller
{
    public function productlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $productdata = Product::select('products.id as product_id','products.id as id','products.name as name','products.image as image','products.description as description','products.category_id as category_id','products.deleted_at as deleted_at','products.created_by as created_by','products.created_at as created_at','products.updated_at as updated_at','products.status as status','products.unique_part_number as unique_part_number','products.vender_part_number as vender_part_number')
            ->where('status','active')
            ->get();
            if(count($productdata)>0){
                $data[]=$productdata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function productdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $product_id = $request->product_id;
        $users= User::where('users.id',$id)->first();

        if($users){
             $productdata = Product::with('categoryName','productImage')
             ->where('id',$product_id)
             ->select('products.id as product_id','products.id as id','products.name as name','products.image as image','products.description as description','products.category_id as category_id','products.deleted_at as deleted_at','products.created_by as created_by','products.created_at as created_at','products.updated_at as updated_at','products.status as status','products.unique_part_number as unique_part_number','products.vender_part_number as vender_part_number')
            ->get();
            if(count($productdata)>0){
                 $data=$productdata;
                 $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

     public function productinquiry(Request $request){
        $data = [];
        $message = "Successfully Done";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $product_id = $request->product_id;
        $name = $request->name;
        $emailid = $request->emailid;
        $description = $request->description;
        $users= User::where('users.id',$user_id)->first();

        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if($product_id==''){
            $status = false;
            $message = 'Enter Tool Id';
            $code = 400;
        }
        if($name==''){
            $status = false;
            $message = 'Enter Name';
            $code = 400;
        }
        if($emailid==''){
            $status = false;
            $message = 'Enter EmailId';
            $code = 400;
        }
        else if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$emailid))
        {
            $message = 'Invalid Mail';
            $status = false;
            $code = 400;
        }

       $productlist = Product::find($product_id);
        if($status){
            if($users){
                if($productlist){
                    $productdata= new Productinquiry;
                    $productdata->user_id=$user_id;
                    $productdata->product_id=$product_id;
                    $productdata->name=$name;
                    $productdata->emailid=$emailid;
                    $productdata->description=$description;
                    $productdata->save();
                }else{
                    $status = false;
                    $message = 'No Tool Found';
                    $code = 400;
                }
            }
            else{
                    $status = false;
                    $message = 'No User Found';
                    $code = 400;
                }
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function toolsAvailableList(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $productdata = Product::with('categoryName','productImage')
            ->where('products.flag_check_in_out','=', 1)
            ->select('products.id as product_id','products.*')
            ->where('products.status','active')
            ->get();

            if(count($productdata)>0){
                $data=$productdata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function checkInOutTools(Request $request){
        $data = [];
        $message = "Tools logs Saved";
        $status = true;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'tool_id'=>'required',
            'in_out_flag'=>'required|integer',
            'date_time'=> 'required|date'//|date_format:Y-m-d H:i:s|before_or_equal:'.date('Y-m-d H:i:s'),
        );



        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            if(empty(json_decode($request->tool_id,1))) {
                $status = false;
                $message = 'The tool id must be an array.';
                $code = 400;
            }
            else {
                $toolids = (array) json_decode(isset($request->tool_id) ? $request->tool_id : '');

                $user_id = $request->user_id;

                $date_time = $request->date_time;
                $in_out_flag = $request->in_out_flag;
                $location = (isset($request->location) ? $request->location : '');
                $users = User::where('users.id',$user_id)->first();



                if(!$users){
                    $status = false;
                    $message = 'No Users Found';
                    $code = 400;
                }
                $remainingArr = array();
                foreach ($toolids as $key => $value) {
                    $product_id = $value;
                    $product = Product::where('id',$product_id)->first();
                    if($status && $product != null){
                        if ($request->in_out_flag == 0) {
                            $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                            if ($productLogs != null) {
                                $remainingArr[] = $product->name;
                                // $status = false;
                                // $message = 'Already Check In';
                                // $code = 400;
                            }else {
                                $product->flag_check_in_out = 0;
                                $product->save();
                                $productLogs = new ProductLogs;
                                $productLogs->product_id = $product_id;
                                $productLogs->user_id = $user_id;
                                $productLogs->start_datetime = $date_time;
                                $productLogs->save();
                                $data = $productLogs;
                            }
                        }
                        else {
                            $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->first();
                            if($productLogs != null)
                            {
                                $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                                if($productLogs != null)
                                {
                                    $product->flag_check_in_out = 1;
                                    $product->save();

                                    $productLogs->location = $location;
                                    $productLogs->end_datetime = $date_time;
                                    $productLogs->save();
                                    $data = $productLogs;
                                }
                                else {
                                    $remainingArr[] = $product->name;
                                    // $status = false;
                                    // $message = 'Already Check Out';
                                    // $code = 400;
                                }
                            }
                            else {
                                $remainingArr[] = $product->name;
                                // $status = false;
                                // $message = 'No Tools Log Found';
                                // $code = 400;
                            }
                        }
                    }
                }


                if (count($remainingArr) > 0) {
                    $stringname =  implode(", ",$remainingArr);
                    $message = $stringname." cannot be ".(($in_out_flag == 0) ? 'check in' : 'check out');
                    $status = true;
                    $code = 200;
                }
                else {
                    $message = "Tools logs Saved";
                    $status = true;
                    $code = 200;
                }
            }
        }
        return response()->json(['data'=>(object)$data, 'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function oldcheckInOutTools(Request $request){
        $data = [];
        $message = "Tools logs Saved";
        $status = true;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'tool_id'=>'required|integer',
            'in_out_flag'=>'required|integer',
            'date_time'=> 'required|date'//|date_format:Y-m-d h:i:s|before_or_equal:'.date('Y-m-d h:i:s'),
        );


        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {

            $user_id = $request->user_id;
            $product_id = $request->tool_id;
            $date_time = $request->date_time;
            $in_out_flag = $request->in_out_flag;

            $users = User::where('users.id',$user_id)->first();
            $product = Product::where('id',$product_id)->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }

            if(!$product){
                $status = false;
                $message = 'No Tool Found';
                $code = 400;
            }

            if($status){
                if ($request->in_out_flag == 0) {
                    $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($productLogs != null) {
                        $status = false;
                        $message = 'Already Check In';
                        $code = 400;
                    }else {
                        $product->flag_check_in_out = 0;
                        $product->save();
                        $productLogs = new ProductLogs;
                        $productLogs->product_id = $product_id;
                        $productLogs->user_id = $user_id;
                        $productLogs->start_datetime = $date_time;
                        $productLogs->save();
                        $data = $productLogs;
                    }
                }
                else {
                    $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->first();
                    if($productLogs != null)
                    {
                        $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                        if($productLogs != null)
                        {
                            $product->flag_check_in_out = 1;
                            $product->save();

                            $productLogs->end_datetime = $date_time;
                            $productLogs->save();
                            $data = $productLogs;
                        }
                        else {
                            $status = false;
                            $message = 'Already Check Out';
                            $code = 400;
                        }
                    }
                    else {
                        $status = false;
                        $message = 'No Tools Log Found';
                        $code = 400;
                    }
                }
            }
        }
        return response()->json(['data'=>(object)$data, 'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function toolsUsedList(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $users= User::where('users.id',$id)->first();
        if($users){
            $productdata = Product::with('categoryName','productImage')
            ->join('product_logs','product_logs.product_id','products.id')
            ->where('product_logs.start_datetime','!=',null)
            ->where('product_logs.end_datetime','=',null)
            ->where('product_logs.user_id','=',$id)
            ->where('products.status','active')
            ->select('products.id as product_id','products.*')
            ->get();
            if(count($productdata)>0){
                $data=$productdata;
                $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Tools Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
}
