<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\Foldervideo;

class FolderVideoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function add(Request $request)
    {
       $video =Video::where('user_type','admin')->where('status','active')->get();
       //print_r($_POST);exit;
       if(!empty($request->input('video_id'))){
            foreach($request->input('video_id') as $video_id){
                $user_id=\Auth::user()->id;
		        $foldervideo = new Foldervideo();
                $foldervideo->folder_id = $request->input('folder_id');
                $foldervideo->job_id = $request->input('job_id');
                $foldervideo->user_type = 'admin';
                $foldervideo->employee_id = $user_id;
                $foldervideo->video_id = $video_id;
                $foldervideo->save();
            }
        }
       return json_encode(array('msg'=>'Success','video'=>$video));
       
       exit;
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
      
        $foldervideo = Foldervideo::find($id);
        $foldervideo->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }  
}
