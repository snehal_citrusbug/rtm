<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Material;
use App\User;
use Session;
use Yajra\Datatables\Datatables;
use DB;
use Auth;
use App\MaterialLogs;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.material.index');
    }
    public function datatable(request $request)
    {
        $material = Material::select('*');

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(material_name LIKE  '%$value%')";

                $material= $material->whereRaw($where_filter);
            }
        }
        $material= $material->orderBy('id','desc')->get();

        return Datatables::of($material)
            ->make(true);
        exit;
    }

    public function logsView($id, Request $request)
    {
        $material = Material::find($id);
        /* Check material is exist or not */


        if($material){
            $userLog = User::select('users.name as user_name','material_logs.*','material.material_name',DB::raw('SUM(material_logs.quantity) as quantity_total'))
            ->join('material_logs','material_logs.user_id','users.id')
            ->join('material','material.id','material_logs.material_id')
            ->where('material_logs.material_id',$id)->where('material_logs.action',1)->groupBy('material_logs.user_id')->get();
            return view('admin.material.logs', compact('material','userLog'));
        }
        else{
             return redirect('/admin/material');
        }
    }

    public function logsDatatable(request $request,$id)
    {
        $materialLog = Material::select('*','users.name as user_name')
        ->join('material_logs','material_logs.material_id','material.id')
        ->join('users','users.id','material_logs.user_id')
        ->where('material_logs.material_id',$id);

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%' OR material_logs.quantity LIKE  '%$value%' OR material.material_name LIKE  '%$value%')";

                $materialLog= $materialLog->whereRaw($where_filter);
            }
        }
        $materialLog = $materialLog->orderBy('material_logs.id','desc')->get();
        //dd($materialLog);
        return Datatables::of($materialLog)
            ->make(true);
        exit;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        return view('admin.material.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
		//$data = $request->all();
        $data['stock'] = isset($request->addstock) ? $request->addstock : 0;
        $data['material_name'] = isset($request->material_name) ? $request->material_name : '';
        $data['min_stock'] = isset($request->min_stock) ? $request->min_stock : 0;


        if ($request->file('image')) {
            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('Materials'), $filename);
            $data['image'] = url('Materials').'/'.$filename;
        }
        else {
            $data['image'] = '';
        }
        $data['created_by'] = \Auth::user()->id;
        $material = Material::create($data);
        if(isset($request->addstock) && $request->addstock != null)
        {
            $materialLogsData['material_id'] = $material->id;
            $materialLogsData['user_id'] = \Auth::user()->id;
            $materialLogsData['quantity'] = $request->addstock;
            $materialLogsData['action'] = 0;
            MaterialLogs::create($materialLogsData);
        }
        Session::flash('flash_message', 'Material added!');

        return redirect('admin/material');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $material = Material::find($id);

        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $material->status= 'inactive';
                $material->update();

                return redirect()->back();
            }else{
                $material->status= 'active';
                $material->update();
                return redirect()->back();
            }

        }
	/* Check material is exist or not */
        if($material){
            return view('admin.material.show', compact('material'));
        }
        else{
             return redirect('/admin/material');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$material = Material::where('id',$id)->first();

        return view('admin.material.edit', compact('material'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rules = [
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        if($request->addstock != null)
        {
            $rules['addstock'] = 'required|integer|min:1';

        }
        $this->validate($request,$rules );


        $requestData = $request->all();

        if ($request->file('image')) {
             $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('Materials'), $filename);
            $data['image'] = url('Materials').'/'.$filename;
        }

        $data['min_stock'] = $request->min_stock;
        $material = Material::findOrFail($id);

        if(isset($request->addstock) && $request->addstock != null)
        {
            $data['stock'] = $material->stock + $request->addstock;

            $materialLogsData['material_id'] = $material->id;
            $materialLogsData['user_id'] = \Auth::user()->id;
            $materialLogsData['quantity'] = $request->addstock;
            $materialLogsData['action'] = 0;
            MaterialLogs::create($materialLogsData);
        }
	    $material->update($data);

        flash('Material Updated Successfully!');

        return redirect('admin/material');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        $material = Material::find($id);
        $material->delete();
        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }
}
