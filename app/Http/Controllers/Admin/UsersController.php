<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Http\Request;
use Session;


class UsersController extends Controller

{

    public function __construct()
    {
        // $this->middleware('permission:access.users');
        // $this->middleware('permission:access.user.edit')->only(['edit', 'update']);
        // $this->middleware('permission:access.user.create')->only(['create', 'store']);
        // $this->middleware('permission:access.user.delete')->only('destroy');
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        /* $customer_id = "";
        if($request->has('customer_id') && $request->get('customer_id') != "" ){

            $customer_id = $request->get('customer_id');
        }
 */
        $role = Role::pluck('label', 'id')->prepend('Filter Users by Roles', '');

        return view('admin.users.index', compact('role'));
    }

    public function datatable(request $request)
    {
        $user = User::select('users.*','roles.name as role_name','roles.id as role_id','roles.label as role_label')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->where('roles.id','=','3')
            ->orderby('users.id','desc');


        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $user->where('users.status', $request->get('filter_status'), 'OR');
        }
       //dd($user->get());
         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%' OR users.email LIKE  '%$value%'  )";
                $user->whereRaw($where_filter);
            }
        }


        $user->groupBy('users.id');

        return Datatables::of($user)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        $roles = Role::where('name','EMP');
        $roles = $roles->pluck('label', 'name');
        $role = $request->role;

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

		$this->validate($request, [
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            // 'email', \Illuminate\Validation\Rule::unique('users', 'email')->Where('deleted_at',NULL),
            'password' => 'required',
        ]);
        $user = User::where('email', $request->input('email'))->withTrashed()->first();

        $data = $request->except('password');
        $data['name'] = isset($request->name) ? $request->name : '';
        $data['password'] = bcrypt($request->password);
		$data['status'] = 'active';
        if($user == null)
        {
            $user = User::create($data);
            $user->assignRole('EMP');
        }
        else
        {
            $user->update($data);
            $user->assignRole('EMP');
            //$role = Role::whereName('SA')->withTrashed()->first();
            //$user->roles()->detach();
            $user->restore();

        }


        Session::flash('flash_message', 'User added!');

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        //notification
        if(\Auth::check()){

            if(\Auth::user()->roles[0]->name == "SU"){
                //if user is added by admin, notification send to all backoffice

                $cbb = Role::Where('name','SU')->get();

                if($cbb != ""){
                    foreach($cbb as $roles){
                        foreach($roles->users as $cbbuser){
                            $user_id[] = $cbbuser->id ;
                        }
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

            }else{

                //if user is added by any backoffice then notification send to admin
                $su = Role::Where('name','SU')->get();

                if($su != ""){
                    foreach($su as $roles){
                        foreach($roles->users as $su_user){
                            $user_id[] = $su_user->id ;
                        }
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

            }

        }

        $user = User::find($id);

        //change users status
        $status = $request->get('status');

        if(!empty($status)){
            if($status == 'active' ){
                $user->status= 'inactive';
                $user->update();
                return redirect()->back();
            }else{
                $user->status= 'active';
                $user->update();
                return redirect()->back();
            }

        }

		if($user){
            return view('admin.users.show', compact('user'));
        }
        else{
             return redirect('/admin/users');
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {

        $request->id=$id;
		$roles = Role::where('name','EMP');
        $roles = $roles->pluck('label', 'name');
		$user = User::where('id',$id)->first();

        return view('admin.users.edit', compact('user', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'email' => 'required|unique:users,email,' . $id
        ]);
        $requestData = $request->all();
        $requestData['name'] = isset($request->name) ? $request->name : '';
        $user = User::findOrFail($id);
	    $user->update($requestData);
        $user->roles()->detach();
        $user->assignRole('EMP');

        flash('User Updated Successfully!');

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $user = User::find($id);
        $user->roles()->detach();
        $user->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

}
