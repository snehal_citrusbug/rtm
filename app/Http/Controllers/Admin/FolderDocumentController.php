<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\Folder;
use App\Folderdocument;
use App\Document;

class FolderDocumentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function add(Request $request)
    {
       $document =Document::where('user_type','admin')->where('status','active')->get();

       if(!empty($request->input('document_id'))){
            foreach($request->input('document_id') as $document_id){
                $user_id=\Auth::user()->id;
		        $folderdocument = new Folderdocument();
                $folderdocument->folder_id = $request->input('folder_id');
                $folderdocument->job_id = $request->input('job_id');
                $folderdocument->user_type = 'admin';
                $folderdocument->employee_id = $user_id;
                $folderdocument->document_id = $document_id;
                $folderdocument->save();
            }
        }
       return json_encode(array('msg'=>'Success','document'=>$document));
       
       exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
      
        $folderdocument = Folderdocument::find($id);
        $folderdocument->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }  
}
