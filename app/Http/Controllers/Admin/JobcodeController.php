<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobcode;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class JobcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.jobcode.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('admin.jobcode.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'job_code' => 'required|unique:job_code,job_code,NULL,id,deleted_at,NULL',
        ]);


        $formdata = Jobcode::where('job_code', $request->input('job_code'))->withTrashed()->first();

        $data['job_code'] = isset($request->job_code) ? $request->job_code : '';
        $data['description'] = isset($request->description) ? $request->description : '';
        if($formdata == null)
        {
            $formdata = Jobcode::create($data);
            $formdata->save();

        }
        else
        {
            $formdata->restore();
            $formdata->update($data);
        }

        Session::flash('flash_message', 'Jobcode added!');

        return redirect('admin/jobcode');
    }

     public function datatable(request $request)
    {
        $job_code = Jobcode::orderBy('job_code.id','desc');

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(job_code.job_code LIKE  '%$value%' OR job_code.description LIKE  '%$value%')";
                $job_code=Jobcode::whereRaw($where_filter);
            }
        }
        //$job_code = $job_code->get();
        //dd($jobcode);
        return Datatables::of($job_code)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $jobcode = Jobcode::find($id);

        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $jobcode->status= 'inactive';
                $jobcode->update();

                return redirect()->back();
            }else{
                $jobcode->status= 'active';
                $jobcode->update();
                return redirect()->back();
            }

        }
		if($jobcode){
            return view('admin.jobcode.show', compact('jobcode'));
        }
        else{
             return redirect('/admin/jobcode');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$jobcode = Jobcode::where('id',$id)->first();

        return view('admin.jobcode.edit', compact('jobcode'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'job_code' => 'required|unique:job_code,job_code,'.$id,
        ]);
        $requestData = $request->all();

        $jobcode = Jobcode::findOrFail($id);

	    $jobcode->update($requestData);
            Session::flash('flash_message', 'Jobcode Updated Successfully!');


        return redirect('admin/jobcode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $jobcode = Jobcode::find($id);
        $jobcode->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }
}
