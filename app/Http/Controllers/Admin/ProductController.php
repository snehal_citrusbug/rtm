<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\ProductLogs;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\Image;
use App\User;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.products.index');
    }

    public function logsView($id, Request $request)
    {
        $product = Product::find($id);

            if($product){
                $productLog = Product::select('*','users.name as user_name')
            ->join('product_logs','product_logs.product_id','products.id')
            ->join('users','users.id','product_logs.user_id')
            ->where('product_logs.product_id',$id);

            // if($request->has('search') && $request->get('search') != '' ){
            //     $search = $request->get('search');
            //     if($search['value'] != ''){
            //         $value = $search['value'];
            //         $where_filter = "(users.name LIKE  '%$value%')";

            //         $productLog= $productLog->whereRaw($where_filter);
            //     }
            // }
            $productLog = $productLog->orderBy('product_logs.id','desc')->get();

             $userLog = User::select('users.name as user_name','product_logs.*')
            ->join('product_logs','product_logs.user_id','users.id')
            ->where('product_logs.end_datetime','!=',null)
            ->where('product_logs.product_id',$id)->get();
            $dataArr = array();
            $userArr = array();
            foreach ($userLog as $key => $value) {
                if(in_array($value->user_id,$userArr))
                {
                    $hourdiff = round((strtotime($value->end_datetime) - strtotime($value->start_datetime))/3600, 1);
                    $hourdiff = $dataArr[$value->user_id]['time'] + $hourdiff;
                    $dataArr[$value->user_id] = array('user_name'=>$value->user_name,'time'=>$hourdiff);
                }
                else {
                   $userArr[] = $value->user_id;
                   $hourdiff = round((strtotime($value->end_datetime) - strtotime($value->start_datetime))/3600, 1);
                   $value->hourdiff = $hourdiff;
                   $dataArr[$value->user_id] = array('user_name'=>$value->user_name,'time'=>$hourdiff);
                }

            }
            return view('admin.products.logs', compact('product','dataArr','productLog'));
        }
        else{
             return redirect('/admin/products');
        }
    }

    public function logsDatatable(request $request,$id)
    {
        $productLog = Product::select('*','users.name as user_name')
        ->join('product_logs','product_logs.product_id','products.id')
        ->join('users','users.id','product_logs.user_id')
        ->where('product_logs.product_id',$id);

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%')";

                $productLog= $productLog->whereRaw($where_filter);
            }
        }
        $productLog = $productLog->orderBy('product_logs.id','desc')->get();

        return Datatables::of($productLog)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        $category = Category::where('status','active')->pluck('name','id')->prepend('Select Category','');
		//dd($category);
		return view('admin.products.create',compact('category'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            // 'name' => 'required',
            // 'description' => 'required',
            // 'category_id' => 'required' ,
            // 'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'vender_part_number'=>'required',
            // 'unique_part_number'=>'required',
            // 'status'=>'required'
        ]);
        $data = $request->all();

        $data['name'] = isset($request->name) ? $request->name : '';
        $data['description'] = isset($request->description) ? $request->description : '';
        $data['category_id'] = isset($request->category_id) ? $request->category_id : '';
        $data['vender_part_number'] = isset($request->vender_part_number) ? $request->vender_part_number : '';
        $data['unique_part_number'] = isset($request->unique_part_number) ? $request->unique_part_number : '';
        $data['image'] = '';

        $product = Product::create($data);

        $files = $request->file('image');

        if ($request->file('image')) {
            $i=1;

                foreach ($files as $image) {

                    $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('Products'), $filename);

                    if ($i == 1)
                        {
                            $product->image = url('Products') . '/' . $filename;
                            $product->save();
                        }

                        $image = Image::create($data);
                        $image['image'] = url('Products').'/'.$filename;
                        $image->product_id=$product->id;
                        $image->save();

                         $i++;
                }

        }


        return redirect('admin/products');
    }

    public function datatable(request $request)
    {
        $product = Product::leftJoin('category', 'products.category_id', '=', 'category.id')
            ->select(['products.*','category.name as cat_name'])
            ->orderBy('products.id','desc');

        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(products.name LIKE '%$value%')";
                $product = Product::join('category', 'products.category_id', '=', 'category.id')
                ->select(['products.*','category.name as cat_name'])->whereRaw($where_filter);
            }
        }

        return Datatables::of($product)
            ->make(true);
        exit;
    }

 /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $product = Product::find($id);

        //change client status
        $status = $request->get('status');
        if (!empty($status)) {
            if ($status == 'active') {
                $product->status = 'inactive';
                $product->update();

                return redirect()->back();
            } else {
                $product->status = 'active';
                $product->update();
                return redirect()->back();
            }

        }

        if($product){
            return view('admin.products.show', compact('product'));
        }
        else{
            return redirect('admin/products');
        }

    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
		$product = Product::where('id',$id)->first();
        $images = Image::where('product_id',$id)->get();
        $category = Category::pluck('name','id');
        return view('admin.products.edit', compact('product','category','images'));
    }


 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[

            // 'name' => 'required',
            // 'description' => 'required',
            // 'category_id' => 'required',
			'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'vender_part_number'=>'required',
            // 'unique_part_number'=>'required',
            // 'status'=>'required'
        ]);
        $requestData = $request->all();

        $requestData['name'] = isset($request->name) ? $request->name : '';
        $requestData['description'] = isset($request->description) ? $request->description : '';
        $requestData['category_id'] = isset($request->category_id) ? $request->category_id : '';
        $requestData['vender_part_number'] = isset($request->vender_part_number) ? $request->vender_part_number : '';
        $requestData['unique_part_number'] = isset($request->unique_part_number) ? $request->unique_part_number : '';

        $product = Product::findOrFail($id);

         $files = $request->file('image');
        if ($request->file('image')) {
            $i=1;
                if(count($files)){
                    $images = Image::where('product_id',$id);
                    $images->delete();
                foreach ($files as $image) {

                    $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('Products'), $filename);

                    if ($i == 1)
                        {

							$requestData['image'] = url('Products').'/'.$filename;
                            $product->update($requestData);

                        }

                        $image = Image::create($requestData);
						$image['image'] = url('Products').'/'.$filename;
                        $image->product_id=$product->id;
                        $image->save();

                         $i++;
                }
                }


        }

	    $product->update($requestData);

        flash('Document Updated Successfully!');

        return redirect('admin/products');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

    $product = Product::find($id);

    $imageurl = explode('/',$product->image);
    $imgname = array_slice($imageurl, 6); // take everything from offset=6 on
    $path = implode('/',$imgname);

    if($product->image){
        //unlink($path);
        }
        $product->delete();

        $images = Image::where('product_id',$id);
        if($images){
            $images->delete();
            $message='Deleted';
        }

        return response()->json(['message'=>$message],200);
    }

    public function checkInOutTools(Request $request){


        $rules = array(
            'product_id'=>'required|integer',
            'in_out_flag'=>'required|in:0,1',
            'date_time'=> 'required|date|date_format:Y-m-d h:i:s|before_or_equal:'.date('Y-m-d h:i:s'),
        );


        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            return response()->json(['message' => $message,'code' => 400], 200);

        } else {

            $user_id = \Auth::user()->id;
            $product_id = $request->product_id;
            $date_time = $request->date_time;
            $in_out_flag = $request->in_out_flag;
            $location = (isset($request->location) ? $request->location : '');

            $product = Product::where('id',$product_id)->first();


                if ($request->in_out_flag == 0) {
                    $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($productLogs != null) {

                        $message = 'Already Check In';
                        return response()->json(['message' => $message, 'code' => 400], 200);

                    }else {
                        $productLogs = new ProductLogs;
                        $productLogs->product_id = $product_id;
                        $productLogs->user_id = $user_id;
                        $productLogs->start_datetime = $date_time;
                        $productLogs->save();
                        return response()->json(['message' => 'Check In Successfully', 'code' => 200], 200);

                    }
                }
                else {
                    $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->first();
                    if($productLogs != null)
                    {
                        $productLogs = ProductLogs::where('product_id', $product_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                        if($productLogs != null)
                        {
                            $productLogs->end_datetime = $date_time;
                            $productLogs->location = ((isset($location) && $location != null) ? $location : '');
                            $productLogs->save();
                            return response()->json(['message' => 'Check Out Successfully', 'code' => 200], 200);

                        }
                        else {
                            $message = 'Already Check Out';
                            return response()->json(['message' => $message, 'code' => 400], 200);

                        }
                    }
                    else {
                        $message = 'No Tools Log Found';
                        return response()->json(['message' => $message, 'code' => 400], 200);

                    }
                }

        };

    }




    }


