<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Jobtask;
use App\Employeejob;

class JobtaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function add(Request $request,$id)
    {
      $user = User::select('users.*' )
            ->join('jobassignemployee','jobassignemployee.job_employee_id','=','users.id')
            ->where('users.status','=','active')
            ->where('jobassignemployee.job_id','=',$id)
            ->get();

            
             if(!empty($request->input('employee_id'))){
                $jobtask = new Jobtask();
                $jobtask->job_id = $request->input('job_id');
                $jobtask->folder_id = $request->input('folder_id');
                $jobtask->title =$request->input('title');
                $jobtask->description =$request->input('description');
                $jobtask->jobtask_status='Pending';    
                $jobtask->save();
                $jobtask->created_by=$jobtask->id;
                $jobtask->save();
                foreach($request->input('employee_id') as $employee_id){

                        $employeejob = new Employeejob();
                        $employeejob->employee_id = $employee_id;
                        $employeejob->job_id = $request->input('job_id');
                        $employeejob->jobtask_id = $jobtask->id;
                        $employeejob->save();
                }
             }
             return json_encode(array('msg'=>'Success','user'=>$user));
       
       exit;
    }
      
     /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
      
        $jobtask = Jobtask::find($id);
        $jobtask->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }  

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $data = Jobtask::where('id',$id)->FirstOrFail();
        if($data){
            $code= 200;
        }else{
            $code = 400;
        }

        $user = User::select('users.*' )
            ->join('jobassignemployee','jobassignemployee.job_employee_id','=','users.id')
            ->where('users.status','=','active')
            ->where('jobassignemployee.job_id','=',$data->job_id)         
            ->get();
        $employees = Employeejob::where('jobtask_id',$id)->get();
        return response()->json(['data' => $data,'employees'=>$employees,'user'=>$user],$code);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function jobtaskupdate($id, Request $request)
    {
   
        if(!empty($request->input('employee_id'))){

                $jobtask = Jobtask::findOrFail($id);
                $jobtask->job_id = $request->input('job_id');
                $jobtask->title =$request->input('title');
                $jobtask->description =$request->input('description');
                $jobtask->update();
                $deleteemp = Employeejob::where('jobtask_id',$id);
                $deleteemp->delete();
               foreach($request->input('employee_id') as $employee_id){

                        $employeejob = new Employeejob();
                        $employeejob->employee_id = $employee_id;
                        $employeejob->job_id = $request->input('job_id');
                        $employeejob->jobtask_id = $jobtask->id;
                        $employeejob->save();
                }
             }
             return json_encode(array('msg'=>'Success'));
       
       exit;
    }  
}
