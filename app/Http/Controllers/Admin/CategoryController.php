<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        return view('admin.category.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'name' => 'required|unique:category,name,NULL,id,deleted_at,NULL',
        ]);

		$data = $request->all();
        $formdata = Category::where('name', $request->input('name'))->withTrashed()->first();

        if($formdata == null)
        {
            $formdata = Category::create($data);
        }
        else
        {
            $formdata->restore();
            $formdata->update($data);
        }

        Session::flash('flash_message', 'Category added!');

        return redirect('admin/category');
    }

     public function datatable(request $request)
    {
        $category = Category::select('*')->orderBy('id','desc');

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(name LIKE  '%$value%')";

                $category=Category::whereRaw($where_filter);
            }
        }
        return Datatables::of($category)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $category = Category::find($id);

        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $category->status= 'inactive';
                $category->update();

                return redirect()->back();
            }else{
                $category->status= 'active';
                $category->update();
                return redirect()->back();
            }

        }
		if($category){
            return view('admin.category.show', compact('category'));
        }
        else{
             return redirect('/admin/category');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$category = Category::where('id',$id)->first();

        return view('admin.category.edit', compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:category',
        ]);
        $requestData = $request->all();

        $category = Category::findOrFail($id);

	    $category->update($requestData);

        flash('Category Updated Successfully!');

        return redirect('admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $category = Category::find($id);
        $category->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

}
