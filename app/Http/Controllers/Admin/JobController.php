<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Job;
use App\Folder;
use App\Video;
use App\Foldervideo;
use App\Jobtask;
use App\Folderdocument;
use App\Client;
use App\Document;
use App\Jobassignemployee;
use Session;
use App\User;
use App\Jobcard;
use App\Builder;
use App\AssignjobBuilder;
use App\Foldernotes;
use App\JobGeothermalForm;
use App\JobLogs;
use DB;
use App\Taskfolder;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.job.index');
    }

    public function archiveJob(Request $request)
    {
        return view('admin.archiveJob.index');
    }

    public function archiveJobDatatable(request $request)
    {
		$job = Job::select('*')->orderBy('id','desc')->where('job.job_status', 'markasdone');
        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(job.title LIKE '%$value%' OR job.job_number LIKE  '%$value%' OR job.duedate LIKE  '%$value%' )";
                $job = $job->whereRaw($where_filter);
            }
        }
        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $job = $job->where('job.status', $request->get('filter_status'));
        }
		// if ($request->has('filter_job_status') && $request->get('filter_job_status') != '' && $request->get('filter_job_status') != 'all') {
        //     $job = $job->where('job.job_status', $request->get('filter_job_status'));
        // }
        return Datatables::of($job)
            ->make(true);
        exit;
    }

    public function logsView($id, Request $request)
    {
        $job = Job::find($id);
        if($job){

            $userLog = User::select('users.name as user_name','job_logs.*')
            ->join('job_logs','job_logs.user_id','users.id')
            ->where('job_logs.end_datetime','!=',null)
            ->where('job_logs.job_id',$id)->get();
            $dataArr = array();
            $userArr = array();
            foreach ($userLog as $key => $value) {
                if(in_array($value->user_id,$userArr))
                {
                    $hourdiff = round((strtotime($value->end_datetime) - strtotime($value->start_datetime))/3600, 1);
                    $hourdiff = $dataArr[$value->user_id]['time'] + $hourdiff;
                    $dataArr[$value->user_id] = array('user_name'=>$value->user_name,'time'=>$hourdiff);
                }
                else {
                   $userArr[] = $value->user_id;
                   $hourdiff = round((strtotime($value->end_datetime) - strtotime($value->start_datetime))/3600, 1);
                   $value->hourdiff = $hourdiff;
                   $dataArr[$value->user_id] = array('user_name'=>$value->user_name,'time'=>$hourdiff);
                }

            }

            return view('admin.job.logs', compact('job','dataArr'));
        }
        else{
             return redirect('/admin/job');
        }
    }

    public function logsDatatable(request $request,$id)
    {
        $jobLog = Job::select('*','users.name as user_name')
        ->join('job_logs','job_logs.job_id','job.id')
        ->join('users','users.id','job_logs.user_id')
        ->where('job_logs.job_id',$id);

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(users.name LIKE  '%$value%')";

                $jobLog= $jobLog->whereRaw($where_filter);
            }
        }
        $jobLog = $jobLog->orderBy('job_logs.id','desc')->get();

        return Datatables::of($jobLog)
            ->make(true);
        exit;
    }

    public function checkInOutJob(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'in_out_flag'=>'required|in:0,1',
            'date_time'=> 'required|date|date_format:Y-m-d h:i:s|before_or_equal:'.date('Y-m-d h:i:s'),
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
            return response()->json(['message' => $message,'code' => 400], 200);

        } else {

            $user_id = \Auth::user()->id;
            $job_id = $request->job_id;
            $date_time = $request->date_time;
            $in_out_flag = $request->in_out_flag;

            $job = Job::where('id',$job_id)->first();


                if ($request->in_out_flag == 0) {
                    $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($jobLogs != null) {

                        $message = 'Already Check In';
                        return response()->json(['message' => $message, 'code' => 400], 200);

                    }else {
                        $jobLogs = new JobLogs;
                        $jobLogs->job_id = $job_id;
                        $jobLogs->user_id = $user_id;
                        $jobLogs->start_datetime = $date_time;
                        $jobLogs->save();
                        return response()->json(['message' => 'Check In Successfully', 'code' => 200], 200);

                    }
                }
                else {
                    $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->first();
                    if($jobLogs != null)
                    {
                        $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                        if($jobLogs != null)
                        {
                            $jobLogs->end_datetime = $date_time;
                            $jobLogs->save();
                            return response()->json(['message' => 'Check Out Successfully', 'code' => 200], 200);

                        }
                        else {
                            $message = 'Already Check Out';
                            return response()->json(['message' => $message, 'code' => 400], 200);

                        }
                    }
                    else {
                        $message = 'No Job Log Found';
                        return response()->json(['message' => $message, 'code' => 400], 200);

                    }
                }

        };

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $client = Client::pluck('name','id')->prepend('Select Client','');
        $builder = Builder::pluck('name','id')->prepend('Select Builder','');
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['EMP'])
            ->where('users.status','=','active')->get();
		return view('admin.job.create',compact('client','user','builder'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        // 'job_number'=>'required',
        // 'title' => 'required',
        // 'description' => 'required',
        // 'client_id'=>'required',
        // 'job_employee_id'  => 'required|array|min:1',

        // 'notes' => 'required',
        $rules = [
            'duedate' => 'required',
            //'job_builder_id'  => 'required',
        ];

        $customMessages = [
            // 'client_id.required' => 'Please Select Client',
            // 'job_employee_id.required' => 'Please Select Employee',
            //'job_builder_id.required' => 'Please Select Builder',
        ];

        $this->validate($request, $rules, $customMessages);
		//$data = $request->all();


        $data['job_number'] = isset($request->job_number) ? $request->job_number : '';
        $data['title'] = isset($request->title) ? $request->title : '';
        $data['description'] = isset($request->description) ? $request->description : '';
        $data['client_id'] = isset($request->client_id) ? $request->client_id : '';
        $data['duedate'] = isset($request->duedate) ? $request->duedate : '';
        $data['notes'] = isset($request->notes) ? $request->notes : '';
        //dd($request->all());
        // if(!empty($request->input('job_employee_id'))){
             //if(!empty($request->input('job_builder_id'))){
                 $job = Job::create($data);
                 $job->created_by=$job->id;
                 $job->status='active';
                 $job->job_status='pending';
                 $job->save();
                //   $assignjobbuilder = new AssignjobBuilder();
                //         $assignjobbuilder->job_id = $job->id;
                //         $assignjobbuilder->job_builder_id = $request->input('job_builder_id');
                //         $assignjobbuilder->save();
                if(!empty($request->input('job_employee_id'))){
                    foreach($request->input('job_employee_id') as $job_employee_id){
                            $jobassignemp = new Jobassignemployee();
                            $jobassignemp->job_id = $job->id;
                            $jobassignemp->job_employee_id = $job_employee_id;
                            $jobassignemp->save();
                    }
                }

                if($request->heating_documents)
                {
                    $documents = $request->heating_documents;
                    foreach ($documents as $key => $value) {
                        $document = $value;
                        $this->documentAdd($document,'2','Heating',$job->id);
                    }
                }
                if ($request->heating_videos) {
                    $videos = $request->heating_videos;
                    foreach ($videos as $key => $value) {
                        $video = $value;
                        $this->videoAdd($video,'2','Heating',$job->id);
                    }
                }

                if($request->cooling_documents)
                {
                    $documents = $request->cooling_documents;
                    foreach ($documents as $key => $value) {
                        $document = $value;
                        $this->documentAdd($document,'3','Cooling',$job->id);
                    }
                }
                if ($request->cooling_videos) {
                    $videos = $request->cooling_videos;
                    foreach ($videos as $key => $value) {
                        $video = $value;
                        $this->videoAdd($video,'3','Cooling',$job->id);
                    }
                }


                if($request->plumbing_documents)
                {
                    $documents = $request->plumbing_documents;
                    foreach ($documents as $key => $value) {
                        $document = $value;
                        $this->documentAdd($document,'4','Plumbing',$job->id);
                    }
                }
                if ($request->plumbing_videos) {
                    $videos = $request->plumbing_videos;
                    foreach ($videos as $key => $value) {
                        $video = $value;
                        $this->videoAdd($video,'4','Plumbing',$job->id);
                    }
                }


             //}

        // }

        Session::flash('flash_message', 'Job added!');

        return redirect('admin/job/'.$job->id.'/edit');
    }

    public function videoAdd($video,$number,$folder_name,$job_id)
    {
        $filename = uniqid(time()) . '.' . $video->getClientOriginalExtension();

        $video->move(public_path('Video'), $filename);
        $data['title'] = $video->getClientOriginalName();
        $data['video_url'] = $filename;
        $data['user_type'] = 'admin';
        $video = Video::create($data);

        $user_id = \Auth::user()->id;

        $folder=Folder::where('job_id',$job_id)->where('folder_name',$folder_name)->first();

        if($folder == null){
            $folder = new Folder();
            $folder->job_id = $job_id;
            $folder->folder_name = $folder_name;
            $folder->sortno = $number;
            $folder->employee_id = $user_id;
            $folder->user_type = 'admin';
            $folder->folder_img = '';
            $folder->save();
            $folder->created_by = $folder->id;
            $folder->save();
        }


        $foldervideo = new Foldervideo();
        $foldervideo->folder_id = $folder->id;
        $foldervideo->job_id = $job_id;
        $foldervideo->user_type = 'admin';
        $foldervideo->employee_id = $user_id;
        $foldervideo->video_id = $video->id;
        $foldervideo->save();
        return $foldervideo;
    }
    public function documentAdd($document,$number,$folder_name,$job_id)
    {
        $filename = uniqid(time()) . '.' . $document->getClientOriginalExtension();

        $document->move(public_path('Document'), $filename);
        $data['title'] = $document->getClientOriginalName();
        $data['document_url'] = $filename;
        $data['user_type'] = 'admin';
        $document = Document::create($data);
        $user_id = \Auth::user()->id;

        $folder=Folder::where('job_id',$job_id)->where('folder_name',$folder_name)->first();

        if($folder == null){
            $folder = new Folder();
            $folder->job_id = $job_id;
            $folder->folder_name = $folder_name;
            $folder->sortno=$number;
            $folder->employee_id=$user_id;
            $folder->user_type='admin';
            $folder->folder_img='';
            $folder->save();
            $folder->created_by=$folder->id;
            $folder->save();
        }

        $folderdocument = new Folderdocument();
        $folderdocument->folder_id = $folder->id;
        $folderdocument->job_id = $job_id;
        $folderdocument->user_type = 'admin';
        $folderdocument->employee_id = $user_id;
        $folderdocument->document_id = $document->id;
        $folderdocument->save();

        return $folderdocument;
    }
    public function datatable(request $request)
    {
		$job = Job::select('*')->orderBy('id','desc');
        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(job.title LIKE '%$value%' OR job.description LIKE  '%$value%'  )";
                $job = $job->whereRaw($where_filter);
            }
        }
        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $job = $job->where('job.status', $request->get('filter_status'));
        }
		if ($request->has('filter_job_status') && $request->get('filter_job_status') != '' && $request->get('filter_job_status') != 'all') {
            $job = $job->where('job.job_status', $request->get('filter_job_status'));
        }
        return Datatables::of($job)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $job = Job::findOrFail($id);
        //change job status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $job->status= 'inactive';
                $job->update();

                return redirect()->back();
            }else{
                $job->status= 'active';
                $job->update();
                return redirect()->back();
            }

        }

        return view('admin.job.show', compact('job','client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {

        $request->id=$id;
		$job = Job::where('id',$id)->first();
        $client = Client::pluck('name','id');
        $folder = Folder::with('documentList','videolist')->where('job_id',$id)->orderby('sortno','asc')->get();
        $taskfolder = Taskfolder::with('taskList')->where('job_id',$id)->orderby('sortno','asc')->get();
        $notes=Foldernotes::with('noteslist')->where('job_id',$id)->orderby('id','desc')->get();
        $jobGeothermalForm = JobGeothermalForm::where('job_id', $id)->orderby('status', 'active')->first();


        $jobtask = Jobtask::where('job_id',$id)->orderby('id','desc')->get();
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['EMP'])
            ->where('users.status','=','active')->get();
        $jobassignemp = Jobassignemployee::where('job_id',$id)->get();
        $jobcarddetail = Jobcard::with('jobcarduserName')->where('job_id',$id)->select('*')->orderBy('id','desc')->get();
        $builder = Builder::pluck('name','id');
        $assignjobbuilder = AssignjobBuilder::where('job_id',$id)->first();
        $job_builder_id=(isset($assignjobbuilder->job_builder_id) ? $assignjobbuilder->job_builder_id : '');

        return view('admin.job.edit', compact('job','client','folder','taskfolder','notes','jobtask','user','jobassignemp','jobcarddetail','job_builder_id','builder','jobGeothermalForm'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rules = [
            // 'job_number'=>'required',
            // 'title' => 'required',
            // 'description' => 'required',
            // 'client_id'=>'required',
            // 'job_employee_id'  => 'required|array|min:1',
             'duedate' => 'required',
            // 'notes' => 'required',
            //'job_builder_id'  => 'required',
        ];

        $customMessages = [
            // 'client_id.required' => 'Please Select Client',
            // 'job_employee_id.required' => 'Please Select Employee',
            //'job_builder_id.required' => 'Please Select Builder',
        ];

        $this->validate($request, $rules, $customMessages);
        $requestData = $request->all();

        $requestData['job_number'] = isset($request->job_number) ? $request->job_number : '';
        $requestData['title'] = isset($request->title) ? $request->title : '';
        $requestData['description'] = isset($request->description) ? $request->description : '';
        $requestData['client_id'] = isset($request->client_id) ? $request->client_id : '';
        $requestData['duedate'] = isset($request->duedate) ? $request->duedate : '';
        $requestData['notes'] = isset($request->notes) ? $request->notes : '';

        $job = Job::findOrFail($id);
	    $job->update($requestData);
        $jobassignemployee = Jobassignemployee::where('job_id',$id);
        $jobassignemployee->delete();

        if(!empty($request->input('job_employee_id'))){
            foreach($request->input('job_employee_id') as $job_employee_id){
                $jobassignemp = new Jobassignemployee();
                $jobassignemp->job_id = $job->id;
                $jobassignemp->job_employee_id = $job_employee_id;
                $jobassignemp->save();
            }
        }
        // $assignjobbuilder = AssignjobBuilder::where('job_id',$id);
        // $assignjobbuilder->delete();
        //                 $assignjobbuilder = new AssignjobBuilder();
        //                 $assignjobbuilder->job_id = $job->id;
        //                 $assignjobbuilder->job_builder_id = $request->input('job_builder_id');
        //                 $assignjobbuilder->save();
        Session::flash('flash_message','Job Updated Successfully!');
        if($job->job_status == 'markasdone')
        {
            return redirect('admin/archiveJob');
        }
        return redirect('admin/job');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $job = Job::find($id);
        $job->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

    public function jobcardlist(Request $request,$id)
    {
        $jobcarddetail = Jobcard::with('jobcarddetailList','jobcarduserName')->where('id',$id)->get();
        return view('admin.job.jobcard',compact('jobcarddetail'));
    }

    public function addJobGeothermalForm(Request $request){

        $rules = array(
            'job_id'=>'required|integer',
        );
        if($request->install_date != null)
        {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if($request->desuperheater != null)
        {
            $rules['desuperheater'] = 'required|in:Y,N';
        }
        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';
        $data = JobGeothermalForm::create($requestData);

        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');


    }
    public function editJobGeothermalForm(Request $request,$id){

        $rules = array();
        if ($request->install_date != null) {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if ($request->desuperheater != null) {
            $rules['desuperheater'] = 'required|in:Y,N';
        }
        $this->validate($request, $rules, []);

        $jobGeothermalForm = JobGeothermalForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');
        $data = $jobGeothermalForm->update($requestData);


        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');


  }

}
