<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends Model
{
    use SoftDeletes;
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'street1', 'street2','country','province','city','email','phone','deleted_at','job_number','zipcode','bill_city','bill_state','bill_zipcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'created_at', 'updated_at', 'created_by', 'updated_by'
    ];
    public function cityName(){
            return $this->belongsTo('App\City','city');
        }

    public function stateName(){
        return $this->belongsTo('App\State','province');
    }
	public function billstateName(){
        return $this->belongsTo('App\State','bill_state');
    }
    public function countryName(){
        return $this->belongsTo('App\Country','country');
    }
    public function assignbuilderid(){
        return $this->hasOne('App\AssignclientBuilder','client_id','id')->with('builderName');
    }
    
}
