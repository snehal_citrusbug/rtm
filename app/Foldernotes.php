<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Foldernotes extends Model
{
      use SoftDeletes;
      public $table='foldernotes';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id','notes','folder_id','employee_id','user_type','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function noteslist(){
            return $this->hasOne
            ('App\User','id','employee_id');
    }
}
