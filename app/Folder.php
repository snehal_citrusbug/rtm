<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    public $table='folder';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id', 'folder_name','created_by','folder_img','sortno'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function documentList(){
            return $this->hasMany
            ('App\Folderdocument','folder_id')->join('documents','documents.id','=','folderdocument.document_id')->join('users','users.id','=','folderdocument.employee_id')->select('folderdocument.id as f_id','folderdocument.folder_id as folder_id','folderdocument.job_id as fjob_id','folderdocument.document_id as document_id','documents.title as doc_title','folderdocument.employee_id as employee_id','folderdocument.user_type as user_type','users.name as user_name','documents.document_url as document_url')->orderby('folderdocument.id','desc');
    }
    public function videoList(){
            return $this->hasMany
            ('App\Foldervideo','folder_id')->join('videos','videos.id','=','foldervideo.video_id')->join('users','users.id','=','foldervideo.employee_id')->select('foldervideo.id as v_id','foldervideo.folder_id as folder_id','foldervideo.job_id as fjob_id','foldervideo.video_id as video_id','videos.title as vid_title','foldervideo.employee_id as employee_id','foldervideo.user_type as user_type','users.name as user_name','videos.video_url as video_url')->orderby('foldervideo.id','desc');
    }
    
    
    

}
