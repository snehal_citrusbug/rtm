<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobcarddetail extends Model
{
   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobcarddetail';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_card_id', 'job_card_name', 'job_card_qty','job_card_price','total','job_card_status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_by','created_at','updated_at','deleted_at'
    ];
}
