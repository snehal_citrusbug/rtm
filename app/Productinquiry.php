<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productinquiry extends Model
{
   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'productinquiry';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'product_id', 'name','emailid','description','status'];
}
