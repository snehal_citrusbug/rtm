<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taskfolder extends Model
{
    public $table='taskfolder';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id', 'folder_name','created_by','folder_img','sortno'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function taskList(){
            return $this->hasMany
            ('App\Jobtask','folder_id')->orderby('jobtask.id','desc');
    }
}
