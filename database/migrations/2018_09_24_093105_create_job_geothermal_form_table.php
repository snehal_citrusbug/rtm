<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobGeothermalFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_geothermal_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('model')->nullable();
            $table->string('serial')->nullable();
            $table->date('install_date')->nullable();
            $table->string('open_closed_loop')->nullable();
            $table->string('desuperheater')->nullable();
            $table->string('source_coax_a')->nullable();
            $table->string('source_coax_b')->nullable();
            $table->string('source_coax_c')->nullable();
            $table->string('source_coax_d')->nullable();
            $table->string('load_coax_a')->nullable();
            $table->string('load_coax_b')->nullable();
            $table->string('load_coax_c')->nullable();
            $table->string('load_coax_d')->nullable();
            $table->string('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_e')->nullable();
            $table->string('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_f')->nullable();
            $table->string('temperature_rise_drop_across_coaxial_heat_exchanger_cooling_g')->nullable();
            $table->string('temperature_rise_drop_across_coaxial_heat_exchanger_heating_e')->nullable();
            $table->string('temperature_rise_drop_across_coaxial_heat_exchanger_heating_f')->nullable();
            $table->string('temperature_rise_drop_across_coaxial_heat_exchanger_heating_g')->nullable();
            $table->string('temperature_rise_drop_across_air_coil_cooling_h')->nullable();
            $table->string('temperature_rise_drop_across_air_coil_cooling_i')->nullable();
            $table->string('temperature_rise_drop_across_air_coil_cooling_j')->nullable();
            $table->string('temperature_rise_drop_across_air_coil_heating_h')->nullable();
            $table->string('temperature_rise_drop_across_air_coil_heating_i')->nullable();
            $table->string('temperature_rise_drop_across_air_coil_heating_j')->nullable();
            $table->string('load_coax_cooling_h')->nullable();
            $table->string('load_coax_cooling_i')->nullable();
            $table->string('load_coax_cooling_j')->nullable();
            $table->string('load_coax_heating_h')->nullable();
            $table->string('load_coax_heating_i')->nullable();
            $table->string('load_coax_heating_j')->nullable();
            $table->string('HR_HE_brine_factor_k')->nullable();
            $table->string('HR_cooling_i')->nullable();
            $table->string('HE_heating_i')->nullable();
            $table->string('watts_cooling_m')->nullable();
            $table->string('watts_cooling_n')->nullable();
            $table->string('watts_cooling_o')->nullable();
            $table->string('watts_heating_m')->nullable();
            $table->string('watts_heating_n')->nullable();
            $table->string('watts_heating_o')->nullable();
            $table->string('capacity_cooling_p')->nullable();
            $table->string('capacity_heating_p')->nullable();
            $table->string('efficiency_cooling_q')->nullable();
            $table->string('efficiency_heating_q')->nullable();
            $table->string('SH_SC_cooling_r')->nullable();
            $table->string('SH_SC_cooling_s')->nullable();
            $table->string('SH_SC_cooling_t')->nullable();
            $table->string('SH_SC_cooling_u')->nullable();
            $table->string('SH_SC_cooling_v')->nullable();
            $table->string('SH_SC_cooling_w')->nullable();
            $table->string('SH_SC_cooling_x')->nullable();
            $table->string('SH_SC_heating_r')->nullable();
            $table->string('SH_SC_heating_s')->nullable();
            $table->string('SH_SC_heating_t')->nullable();
            $table->string('SH_SC_heating_u')->nullable();
            $table->string('SH_SC_heating_v')->nullable();
            $table->string('SH_SC_heating_w')->nullable();
            $table->string('SH_SC_heating_x')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_geothermal_form');

    }
}
