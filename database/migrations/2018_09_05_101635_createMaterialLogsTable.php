<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('material_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id');
            $table->integer('user_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('action')->comment('0-added,1-used');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_logs');
    }
}
