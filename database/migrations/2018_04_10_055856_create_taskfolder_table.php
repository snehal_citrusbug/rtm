<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskfolderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('taskfolder', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->nullable();
            $table->string('folder_name');
            $table->string('status')->default('active')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('folder_img')->nullable();
            $table->string('user_type')->nullable();
            $table->integer('employee_id')->nullable();
            $table->integer('sortno')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taskfolder');
    }
}
