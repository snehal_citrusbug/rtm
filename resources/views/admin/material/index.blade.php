@extends('layouts.backend')

@section('title',trans('material.materials'))
@section('pageTitle',trans('material.materials'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('material.materials')
                    </div>
                </div>
                <div class="box-content ">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/admin/material/create') }}" class="btn btn-success btn-sm"
                                title="Add New material">
                                <i class="fa fa-plus" aria-hidden="true"></i> @lang('material.add_new_material')
                            </a>
                        </div>
                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/material', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}

                            {!! Form::close() !!}
                            </div>
                        </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="material-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('material.id')</th> -->
                                <th data-priority="3">@lang('material.material_name')</th>
                                <th data-priority="3">@lang('material.min_stock')</th>
                                <th data-priority="7">@lang('material.stock')</th>
                                <th data-priority="5">@lang('product.image')</th>
                                <th data-priority="8">@lang('material.status')</th>
                                <th data-priority="8">@lang('material.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/material/') }}";
      var dataurl ="{{ url('/admin/material/datatable') }}";
        datatable = $('#material-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: dataurl,
                    type: "get", // method , by default get

                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'material_name',name:'material_name',"searchable" : true},
                    { data: 'min_stock',name:'min_stock',"searchable" : true},
                    { data: 'stock',name:'stock',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

							var img=o.image;
							if(img){
								return '<a href="'+o.image+'" target="_blank" ><img src="'+o.image+'" class="product_thumb"></a>';
                            }else{
								return 'No Image';
							}
                        }
			        },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status=='blocked')
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('material.blocked')</button></a>";
                            else if(o.status == 'inactive')

                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("material.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('material.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('material.edit')</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('material.delete')</button></a>&nbsp;";

                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('material.view')</button></a>&nbsp;";

                            var l =  "<a href='"+url+"/logs/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-list' aria-hidden='true'></i> @lang('material.logs')</button></a>&nbsp;";

                            return v+e+d+l;
                        }
                    }
                ]
        });

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/material/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete material ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
