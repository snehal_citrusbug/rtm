@extends('layouts.backend')

@section('title',trans('material.materials'))
@section('pageTitle',trans('material.materials'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('material.materials') @lang('material.logs')
                    </div>
                </div>

                <div class="box-content ">
                    <div class="row">
                        <a href="{{ url('/admin/material') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('material.back')
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="material-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('material.id')</th> -->
                                <th data-priority="3">@lang('material.material_name')</th>
                                <th data-priority="3">@lang('material.user_name')</th>
                                <th data-priority="7">@lang('material.quantity')</th>
                                <th data-priority="7">@lang('material.description')</th>
                                <th data-priority="8">@lang('material.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="box-content ">
                    <div class="table-responsive">
                        <table class="table table-borderless" id="material-user-table">
                            <thead>
                                <tr>
                                    <th data-priority="3">@lang('material.material_name')</th>
                                    <th data-priority="3">@lang('material.user_name')</th>
                                    <th data-priority="7">@lang('material.total_quantity')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($userLog) > 0)
                                    @foreach ($userLog as $item)
                                    <tr>
                                        <td>{{$item->material_name}}</td>
                                        <td>{{$item->user_name}}</td>
                                        <td>{{$item->quantity_total}}</td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                @endif
                                </tbody>
                        </table>
                    </div>
                </div>



            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
      var dataurl ="{{ url('/admin/material/logs/datatable') }}";
        datatable = $('#material-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: dataurl+'/'+"{{$material->id}}",
                    type: "get", // method , by default get

                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'material_name',name:'material_name',"searchable" : true},
                    { data: 'user_name',name:'user_name',"searchable" : true},
                    { data: 'quantity',name:'quantity',"searchable" : true},
                    { data: 'description',name:'description',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var action = '';
                            if(o.action=='0')
                            action = "<label>@lang('material.added')</label>";
                            else
                            action = "<label> @lang('material.used')</label>";
                            return action;

                        }

                    }
                ]
        });


</script>
@endpush
