@extends('layouts.backend')

@section('title',trans('builder.edit_builder'))
@section('pageTitle',trans('builder.edit_builder'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('builder.edit_builder')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('builder.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($builder, [
                        'method' => 'PATCH',
                        'url' => ['/admin/builder', $builder->id],
                        'class' => 'form-horizontal',
                        'id'=>'formBuilder',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.builder.form', ['submitButtonText' => trans('builder.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
