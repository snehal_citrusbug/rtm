
<div class="form-group{{ $errors->has('job_code') ? ' has-error' : ''}}">
    {!! Form::label('job_code', trans('jobcode.job_code'), ['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::text('job_code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('job_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', trans('jobcode.description'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('description', null, ['class' => 'form-control']) !!} {!! $errors->first('description', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('jobcode.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

