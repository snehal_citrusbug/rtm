@extends('layouts.backend')


@section('title',trans('dashboard.label.dashboard'))
<style>
.red{
  color:red;
}
input#j_title.red-border, input#a_date.red-border, input#s_time.red-border, input#e_time.red-border, select#eid.red-border
{
  border:1px solid red !important;
}

</style>  
@push('js')
<script src="https://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>

@endpush

@section('content')

    <div class="box bordered-box blue-border">
        <div class="box-header blue-background">
            <div class="title">
                <i class="icon-circle-blank"></i>
                @lang('dashboard.label.dashboard')
            </div>
        </div>
        <div class="panel-body" id="view_msg">
           <button id="assign_job" class="btn btn-success btn-xs create-job">Assign Job</button>
           <div class="col-md-3 pull-right">
           <form name="filterjob" action="{{url('/')}}" method="get">
           <select class="form-control" name="emp_id" id="emp_id" required>
              <option value="0">--select employee</option>
              @foreach($employees as $employee)
                    <option value="{{$employee->id}}">{{$employee->name}}</option>
              @endforeach
            </select>   
            <input type="submit" name="assign" value="filter" class="btn btn-success btn-xs"/>
            </form>
            </div>
        </div>
        <div class="panel-body">
          <div class="row">
            <div id="wrapper">
              <div id="myScheduler"></div>
              
            </div>
          </div>
        </div>
    </div>
    <div id="dialog-form" title="Assign Job">
      <form name="assign_form" class="form-horizontal">
      <fieldset>
          <label for="eid" class="control-label">Employee Name<span class="red">*</span></label>
          <select class="form-control" name="job_employee_id" id="eid" required>
            <option value="0">--select--</option>
            @foreach($employees as $employee)
                  <option value="{{$employee->id}}">{{$employee->name}}</option>
            @endforeach
          </select>               
          
          <label for="j_title" class="control-label">Job Title<span class="red">*</span></label>
          <input type="text" name="title" id="j_title" class="form-control" required  placeholder="Enter Job Title">
          
          <label for="j_desc" class="control-label">Job Description</label>
          <textarea name="description" id="j_desc" class="form-control"></textarea>
          
          <label for="a_date" class="control-label">Date<span class="red">*</span></label>
          <input type="text" name="duedate" id="a_date" class="form-control" required>

          <label for="s_time" class="control-label">Start Time(HH:MM PM/AM)<span class="red">*</span></label>
          <input type="time" name="start_time" id="s_time" class="form-control" required>

            <label for="e_time" class="control-label">End Time(HH:MM PM/AM)<span class="red">*</span></label>
          <input type="time" name="end_time" id="e_time" class="form-control" required>


            <!-- Allow form submission with keyboard without duplicating the dialog button -->
          <input type="submit" tabindex="-1" style="position:absolute; top:-1000px" class="form-control">
      </fieldset>
      </form>
      <p class="validateTips" style="color:red;">All form fields are required.</p>
      </div>

@endsection
@push('js')
<script>
YUI().use(
  'aui-scheduler',
  function(Y) {

   // var data = {{ $events }};
  //  console.log(data); 
  var events =  JSON.parse('{{ $events }}'.replace(/&quot;/g,'"'));
  console.log(events); 
  var tmp = [];
  for(i=0;i<events.length;i++){

    enddatearray =   events[i].endDate.split(',');
    startdatearray =   events[i].startDate.split(',');

      t = {
        content: events[i].content,
        endDate: new Date(enddatearray[0],enddatearray[1],enddatearray[2],enddatearray[3],enddatearray[4],),
        startDate: new Date(startdatearray[0],startdatearray[1],startdatearray[2],startdatearray[3],startdatearray[4],),
      };

      tmp.push(t);
  }
  events = tmp; 
  // var events = [
  //     {
  //       content: 'AllDay',
  //       endDate: new Date(2018, 1, 5, 18, 00),
  //       startDate: new Date(2018, 1, 5, 7, 00)
  //     }
  // ];
     

    //console.log(events); 

    var agendaView = new Y.SchedulerAgendaView();
    var dayView = new Y.SchedulerDayView();
    var eventRecorder = new Y.SchedulerEventRecorder();
    var monthView = new Y.SchedulerMonthView();
    var weekView = new Y.SchedulerWeekView();

    new Y.Scheduler(
      {
        boundingBox: '#myScheduler',
        date: new Date(),
        items: events,
        render: true,
		//eventRecorder: eventRecorder,
        views: [weekView, monthView]
      }
    );


  }
);
</script>
<script>
          $( function() {
            var dialog, form,
         
            eid = $("#eid"),
            j_title = $("#j_title"),
            j_desc = $("#j_desc"),
            a_date = $("#a_date"),
            s_time = $("#s_time"),
            e_time = $("#e_time"),
            allFields = $( [] ).add(eid).add(j_title).add(j_desc).add(a_date).add(s_time).add(e_time),
            tips = $( ".validateTips" );
         
            function updateTips( t ) {
              tips
                .text( t )
                .addClass( "ui-state-highlight" );
              setTimeout(function() {
                tips.removeClass( "ui-state-highlight", 1500 );
              }, 500 );
            }
         
            
            function addJob() {
                  eid = $("#eid").val();
                  j_title = $("#j_title").val();
                  j_desc = $("#j_desc").val();
                  a_date = $("#a_date").val();
                 // dateAr = change_date_format.split('-');
                 // a_date = dateAr[2] + '-' + dateAr[0] + '-' + dateAr[1];
                 // a_date = $("#a_date").val(change_date);
                  s_time = $("#s_time").val();
                  e_time = $("#e_time").val();
                  console.log(a_date);
                    if(j_title == ""){
                       $('#j_title').addClass('red-border');
                       return false;
                    }
                    else if(a_date == ""){
                      $('#a_date').addClass('red-border');
                      return false;
                    }
                    else if(s_time == ""){
                      $('#s_time').addClass('red-border');
                      return false;
                    }
                    else if(e_time == ""){
                      $('#e_time').addClass('red-border');
                      return false;
                    }
                    else if(eid == 0){
                      $('#eid').addClass('red-border');
                      return false;
                    }
                    else{
                      $('#e_time').removeClass('red-border');
                      $('#eid').removeClass('red-border');
                      $('#j_title').removeClass('red-border');
                      $('#a_date').removeClass('red-border');
                      $('#s_time').removeClass('red-border');
                    }
                    
                 
                    $.ajax({
                      type: "GET",
                      url: '{{url("admin/assignjob/")}}',
                      data: {title: j_title, description: j_desc, duedate: a_date, start_time: s_time,
                       end_time:e_time, job_employee_id:eid  },
                      success: function( msg ) {
                        window.location = "{{url('/')}}";
                        dialog.dialog( "close" );
                      }
                  });
                 
                  
                 
              
            
            }
         
            dialog = $( "#dialog-form" ).dialog({
              autoOpen: false,
              height: 400,
              width: 350,
              modal: true,
              buttons: {
                "Save": addJob,
                 Cancel: function() {
                  dialog.dialog( "close" );
                }
              },
              close: function() {
                form[ 0 ].reset();
                allFields.removeClass( "ui-state-error" );
              }
            });
          
            form = dialog.find( "form" ).on( "submit", function( event ) {
              event.preventDefault();
              addJob();
            });
         
            $( ".create-job" ).button().on( "click", function() {
              dialog.dialog( "open" );
              document.getElementsByClassName("ui-dialog-titlebar-close")[0].innerHTML = "<i class='fa fa-close' aria-hidden='true'></i>";
            
          });
              $("#a_date").datepicker({
                  minDate: 0,
                  dateFormat: 'mm-dd-yy',
                  // dateFormat: 'yy-mm-dd',
              });

              
              // $('#emp_id').on('change', function() {
              //     //alert( $(this).find(":selected").val() );
              //     var emp_id = $(this).val();
              //     $.ajax({
              //         type: "GET",
              //         url: '{{url("admin/employeejobschedule/")}}',
              //         data: {emp_id: emp_id },
              //         success: function( msg ) {
              //           $("#view_msg").append("<span class='alert alert-success'>"+msg+"</span>"); 
              //           dialog.dialog( "close" );
              //         }
              //     });
              // });
          } );
          
</script>

@endpush
