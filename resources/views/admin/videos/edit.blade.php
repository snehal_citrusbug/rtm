@extends('layouts.backend')

@section('title',trans('video.edit_video'))
@section('pageTitle',trans('video.edit_video'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('video.edit_video')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('video.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($video, [
                        'method' => 'PATCH',
                        'url' => ['/admin/videos', $video->id],
                        'class' => 'form-horizontal',
                        'id'=>'formVideo',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.videos.form', ['submitButtonText' => trans('video.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
