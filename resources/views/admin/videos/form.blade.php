
<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', trans('video.title'), ['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('video.description'), ['class' => 'col-md-4 control-label ']) !!}
     <div class="col-md-6">
   {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@if(isset($video) && $video->video_url)


<div class="form-group{{ $errors->has('video_url') ? ' has-error' : ''}}">
   {!! Form::label('video_url', trans('video.changevideo'),['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::file('video_url', null, ['class' => 'form-control']) !!}
            {!! $errors->first('video_url', '<p class="help-block with-errors">:message</p>') !!}
    </div>
	<a class="down_doc" href="{!! asset('Video/'.$video->video_url) !!}" target="_blank">Download here</a>
</div>
@else
<div class="form-group{{ $errors->has('video_url') ? ' has-error' : ''}}">
   {!! Form::label('video_url', trans('video.addvideo'),['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::file('video_url', null, ['class' => 'form-control']) !!}
            {!! $errors->first('video_url', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('client.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

