@extends('layouts.backend')

@section('title',trans('category.view_category'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('category.category')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('category.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/category/' . $category->id . '/edit') }}" title="Edit Category">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('category.edit_category')
                        </button>
                    </a>
                    @endif
                    
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('category.id')</td>
                                <td>{{ $category->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('category.name')</td>
                                <td>{{ $category->name }}</td>
                            </tr>
                       
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection