@extends('layouts.backend')

@section('title',trans('job.jobs'))
@section('pageTitle',trans('job.jobs'))

@section('content')
	<div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('job.jobs')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/job/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Job">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('job.add_new_job')
                                </a>

                        </div>

                        <div class="col-md-3">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/job', 'class' => 'form-horizontal', 'role' => 'search'])  !!}
                                <div class="form-group">
								<label for="filter_status" class="col-md-4 control-label ">Status</label>
								<div class="col-md-6">
                            {!! Form::select('status',array('active'=>'Active','inactive'=>'Inactive','all'=>'All'),'',['class'=>'form-control ', 'id'=>'filter_status']) !!}
								</div>
                            </div>
                            {!! Form::close() !!}
                            </div>

						<div class="col-md-3">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/job', 'class' => 'form-horizontal', 'role' => 'search'])  !!}
                                <div class="form-group">
								<label for="filter_job_status" class="col-md-4 control-label ">Job Status</label>
								<div class="col-md-6">
                            {!! Form::select('status',array('all'=>'All','markasdone'=>'Done','pending'=>'Pending',),'',['class'=>'form-control', 'id'=>'filter_job_status']) !!}
							</div>
                            </div>
                            {!! Form::close() !!}
                            </div>
                        </div>





                    <div class="table-responsive">
                        <table class="table table-borderless" id="jobs-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('job.id')</th> -->
                                <th data-priority="1">@lang('job.title')</th>
                                <th data-priority="2">@lang('job.description')</th>
								<th data-priority="4">@lang('job.jobStatus')</th>
                                <th data-priority="5">@lang('job.status')</th>
                                <th data-priority="3">@lang('job.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="checkInOutJobView" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check In Out Job</h4>
                </div>
                <div class="modal-body">
                    {!! Form::hidden('job_id', null, ['class' => 'form-control','id' => 'job_id']) !!}
                    {!! Form::hidden('in_out_flag', null, ['class' => 'form-control','id' => 'in_out_flag']) !!}
                    <div class="form-group{{ $errors->has('checkinoutdatetime') ? ' has-error' : ''}}">
                        {!! Form::label('checkinoutdatetime', trans('job.checkinoutdatetime'), ['class' => 'col-md-4 control-label required']) !!}
                        <div class="col-md-6">
                            {!! Form::text('checkinoutdatetime', null, ['class' => 'form-control', 'id' => 'checkinoutdatetime']) !!}
                            <p style="color:red" class="help-block checkinoutdatetimeerr"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success checkInOutJob">Save</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('script-head')
<script>

    $('.checkinoutdatetimeerr').html('');
    $('#checkinoutdatetime').datetimepicker({
        format: 'YYYY-MM-DD hh:mm:ss',

    });
var url ="{{ url('/admin/job/') }}";

        datatable = $('#jobs-table').DataTable({
            //"order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('JobControllerJobsData') !!}',
                    type: "get", // method , by default get
                    data: function (d) {
                        d.filter_status = $('#filter_status').val();
                        d.filter_job_status = $('#filter_job_status').val();
                    }
                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'title',name:'title',"searchable" : false},
                    { data: 'description',name:'jobs.description',"searchable" : false},
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.job_status == 'markasdone')

								status = "Done";
                            else
								status = "Pending";

                            return status;

                        }
					},


                     {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.status == 'inactive')

                             status = "<a href='"+url+"/"+o.id+"?status=inactive' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('job.inactive')</button></a>";
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('job.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e=""; var d=""; var inJob=''; var outJob='';
                                // /"+url+"/"+o.id+"/0/checkInOutJob
                                inJob= "<a href='#' data-inout='0' data-id="+o.id+" class='checkInOutJobView'><button class='btn btn-success btn-xs' type='button' data-toggle='modal'  data-target='#checkInOutJobView' >Check In</button></a>&nbsp;";

                                outJob= "<a href='#' data-inout='1' class='checkInOutJobView' data-id="+o.id+"><button class='btn btn-success btn-xs' type='button' data-toggle='modal'  data-target='#checkInOutJobView'>Check Out </button></a>&nbsp;";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";

                                var l = "<a href='"+url+"/logs/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-list' aria-hidden='true'></i> @lang('material.logs')</button></a>&nbsp;";
                           // var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('job.view')</button></a>&nbsp;";


                            return e+d+l+inJob+outJob;
                        }
                    }

                ]
        });

    $(document).on('click', '.checkInOutJobView', function (e) {
        var job_id = $(this).attr('data-id');
        var in_out_flag = $(this).attr('data-inout');
        $('#job_id').val(job_id);
        $('#in_out_flag').val(in_out_flag);
    });
    $(document).on('click', '.checkInOutJob', function (e) {
        var datetime = $('#checkinoutdatetime').val();
        if(datetime != '' && datetime != null)
        {
            $('#checkInOutJobView').modal('hide');
            $('.checkinoutdatetimeerr').html('');
            var job_id = $('#job_id').val();
            var in_out_flag = $('#in_out_flag').val();
            var url ="{{ url('/admin/job/checkInOutJob') }}";

            var data = {'datetime' : datetime}
            $.ajax({
                type: "post",
                url: url ,
                data : {'date_time' : datetime,'job_id':job_id,'in_out_flag':in_out_flag},
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    if(data.code == 400)
                    {
                        toastr.error('Action Not Procede!', data.message);
                    }
                    else
                    {
                        toastr.success('Action Success!', data.message);
                    }

                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        else
        {
            $('.checkinoutdatetimeerr').html('Please Select Date Time.')
        }

    });
    $('#filter_status').change(function() {
        datatable.draw();
    });
	$('#filter_job_status').change(function() {
        datatable.draw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/job/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete Job ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
