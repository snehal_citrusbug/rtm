@extends('layouts.backend')
@section('title',trans('job.job'))
@section('pageTitle',trans('job.job'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('job.job') @lang('job.logs')
                    </div>
                </div>

                <div class="box-content ">
                    <div class="row">
                        <a href="{{ url('/admin/job') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('job.back')
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="job-table">
                            <thead>
                            <tr>
                                <th data-priority="3">@lang('job.user_name')</th>
                                <th data-priority="7">@lang('job.start_datetime')</th>
                                <th data-priority="7">@lang('job.end_datetime')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>

                <div class="box-content ">
                    <div class="table-responsive">
                        <table class="table table-borderless" id="product-table">
                            <thead>
                                <tr>
                                    <th data-priority="1">@lang('product.user_id')</th>
                                    <th data-priority="3">@lang('product.user_name')</th>
                                    <th data-priority="7">@lang('product.hour')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($dataArr) > 0) @foreach ($dataArr as $key => $item)
                                <tr>
                                    <td>{{$key}}</td>
                                    <td>{{$item['user_name']}}</td>
                                    <td>{{$item['time'].' '.(($item['time'] > 1) ? 'Hours' : 'Hour') }} </td>
                                </tr>
                                @endforeach @else
                                <tr>
                                    <td colspan="3">No data available in table</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
      var dataurl ="{{ url('/admin/job/logs/datatable') }}";
        datatable = $('#job-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: dataurl+'/'+"{{$job->id}}",
                    type: "get", // method , by default get

                },
                columns: [
                    { data: 'user_name',name:'user_name',"searchable" : true},
                    { data: 'start_datetime',name:'start_datetime',"searchable" : true},
                    { data: 'end_datetime',name:'end_datetime',"searchable" : true}

                ]
        });


</script>
@endpush
