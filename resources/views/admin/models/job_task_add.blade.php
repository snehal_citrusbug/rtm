<!-- Add Cash Add Modal -->
<div class="modal fade " id="jobtaskAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title jobtask-title" id="account_form_model_lable">Add Job Task</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="job_task_add_form" name="form">
                                  <input  name="job_id"  id="job_id" type="hidden" value="{{$job->id}}" >
                                  <input  name="updjobtaskid"  id="updjobtaskid" type="hidden" value="" >
                                   <input  name="updjobfoldertaskid"  id="updjobfoldertaskid" type="hidden" value="" >
                                  
                                  <input  name="folder_id"  id="taskfolderid" type="hidden" value="" >
                                  
                                 
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                       
                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">Employee Name</label>
                                             <select id="employee_id" class="form-control" name="employee_id[]" multiple>
                                            </select>
                                              <div class="employee_error"></div>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">Title</label>
                                              <input  name="title"  id="jobtasktitle" type="text" value="" class="form-control">
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">Description</label>
                                              <textarea  name="description"  id="jobtaskdescription" rows="5" cols="30"  class="form-control" value="" ></textarea>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="Submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
