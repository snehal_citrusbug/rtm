@extends('layouts.backend')
@section('title',trans('product.products'))
@section('pageTitle',trans('product.products'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('product.products') @lang('product.logs')
                    </div>
                </div>

                <div class="box-content ">
                    <div class="row">
                        <a href="{{ url('/admin/products') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('product.back')
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="product-table">
                            <thead>
                            <tr>
                                <th data-priority="3">@lang('product.user_name')</th>
                                <th data-priority="7">@lang('product.start_datetime')</th>
                                <th data-priority="7">@lang('product.end_datetime')</th>
                                <th data-priority="7">@lang('job.location')</th>
                            </tr>
                            </thead>
                             <tbody>
                                @if(count($productLog) > 0)
                                @foreach ($productLog as $item)
                                <tr>
                                    <td>{{$item->user_name}}</td>
                                    <td>{{$item->start_datetime}}</td>
                                    <td>{{$item->end_datetime}}</td>
                                    <td>{{$item->location}}</td>
                                </tr>
                                @endforeach @else
                                <tr>
                                    <td colspan="3">No data available in table</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="box-content ">
                    <div class="table-responsive">
                        <table class="table table-borderless" id="product-table">
                            <thead>
                                <tr>
                                    <th data-priority="1">@lang('product.user_id')</th>
                                    <th data-priority="3">@lang('product.user_name')</th>
                                    <th data-priority="7">@lang('product.hour')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($dataArr) > 0)
                                @foreach ($dataArr as $key => $item)
                                <tr>
                                    <td>{{$key}}</td>
                                    <td>{{$item['user_name']}}</td>
                                    <td>{{$item['time'].' '.(($item['time'] > 1) ? 'Hours' : 'Hour') }} </td>
                                </tr>
                                @endforeach @else
                                <tr>
                                    <td colspan="3">No data available in table</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
    //   var dataurl ="{{ url('/admin/products/logs/datatable') }}";
    //     datatable = $('#product-table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //          ajax: {
    //                 url: dataurl+'/'+"{{$product->id}}",
    //                 type: "get",

    //             },
    //             columns: [
    //                 { data: 'user_name',name:'user_name',"searchable" : true},
    //                 { data: 'start_datetime',name:'start_datetime',"searchable" : true},
    //                 { data: 'end_datetime',name:'end_datetime',"searchable" : true},
    //                 { data: 'location',name:'location',"searchable" : true}
    //             ]
    //     });


</script>
@endpush
