@extends('layouts.backend')

@section('title',trans('sadmin.view_user'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('sadmin.user')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('sadmin.back')
                        </button>
                    </a>
                     @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/sadmin/' . $user->id . '/edit') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('sadmin.edit_user')
                        </button>
                    </a>
                    @endif
                    <br/>
                    <br/>


                    <?php
                    $role = join(' + ', $user->roles()->pluck('label')->toArray());
                    $rolename = join(' + ', $user->roles()->pluck('name')->toArray());
                    ?>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('sadmin.id')</td>
                                <td>{{ $user->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('sadmin.name')</td>
                                <td>{{ $user->name }}</td>
                            </tr>
                       
                            <tr>
                                <td>@lang('sadmin.email')</td>
                                <td>{{ $user->email }}</td>
                            </tr>

                            <tr>
                                <td>@lang('sadmin.role')</td>
                                <td>{{ $role }}</td>
                            </tr>                                             
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection