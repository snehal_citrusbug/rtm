@extends('layouts.backend')

@section('title',trans('document.edit_document'))
@section('pageTitle',trans('document.edit_document'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('document.edit_document')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('document.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($document, [
                        'method' => 'PATCH',
                        'url' => ['/admin/documents', $document->id],
                        'class' => 'form-horizontal',
                        'id'=>'formDocument',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.documents.form', ['submitButtonText' => trans('document.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
