
<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', trans('document.title'), ['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('document.description'), ['class' => 'col-md-4 control-label ']) !!}
     <div class="col-md-6">
     {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@if(isset($document) && $document->document_url)


<div class="form-group{{ $errors->has('document_url') ? ' has-error' : ''}}">
    {!! Form::label('document_url', trans('document.changedocument'),['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::file('document_url', null, ['class' => 'form-control']) !!} {!! $errors->first('document_url', '
        <p class="help-block with-errors">:message</p>') !!}
    </div>
    <a class="down_doc" href="{!! asset('Document/'.$document->document_url) !!}" target="_blank">Download here</a>
</div>
@else
<div class="form-group{{ $errors->has('document_url') ? ' has-error' : ''}}">
    {!! Form::label('document_url', trans('document.adddocument'),['class' => 'col-md-4 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::file('document_url', null, ['class' => 'form-control']) !!} {!! $errors->first('document_url', '
        <p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('document.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

