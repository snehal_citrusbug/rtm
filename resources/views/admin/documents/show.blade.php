@extends('layouts.backend')

@section('title',trans('document.view_document'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('document.document')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('document.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/documents/' . $document->id . '/edit') }}" title="Edit Document">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('document.edit_document')
                        </button>
                    </a>
                    @endif
                   
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('document.id')</td>
                                <td>{{ $document->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('document.title')</td>
                                <td>{{ $document->title }}</td>
                            </tr>
                       
                            <tr>
                                <td>@lang('document.description')</td>
                                <td>{{ $document->description }}</td>
                            </tr>

                            <tr>
                                <td>Document</td>
                                <td><a href="{{ url('Document') }}/{{ $document->document_url }}" target="_blank">Download here<a></td></a>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection