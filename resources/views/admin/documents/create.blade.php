@extends('layouts.backend')


@section('title',trans('document.add_document'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('document.add_new_document')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('document.add_new_document')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('document.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/documents', 'class' => 'form-horizontal','id'=>'formDocument','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.documents.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

