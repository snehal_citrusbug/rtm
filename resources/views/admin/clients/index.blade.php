@extends('layouts.backend')

@section('title',trans('client.clients'))
@section('pageTitle',trans('client.clients'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('client.clients')
                    </div>
                </div>
                <div class="box-content ">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ url('/admin/clients/create') }}" class="btn btn-success btn-sm"
                                title="Add New Client">
                                <i class="fa fa-plus" aria-hidden="true"></i> @lang('client.add_new_client')
                            </a>
                                <a href="{{ url('/admin/clients/downloadsamplecsv') }}" class="btn btn-success btn-sm"
                                title="Download Sample Csv">
                                <i class="fa fa-plus" aria-hidden="true"></i> Sample Csv
                            </a>
                            <a href="{{ url('/admin/clients/createclient') }}" class="btn btn-success btn-sm" title="Upload Csv">
                                <i class="fa fa-plus" aria-hidden="true"></i> Upload csv
                            </a>
                        </div>
                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/clients', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="clients-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('client.id')</th>-->
                                <th data-priority="3">@lang('client.name')</th>
                                {{-- <th data-priority="4">@lang('client.buildername')</th> --}}
                                <th data-priority="5">@lang('client.job_number')</th>
                                <th data-priority="7">@lang('client.status')</th>
                                <th data-priority="8">@lang('client.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/clients/') }}";

        datatable = $('#clients-table').DataTable({
           //"order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('ClientControllerClientsData') !!}',
                    type: "get", // method , by default get

                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'name',name:'clients.name',"searchable" : true},
                    // {
                    //     "data": null,
                    //     "searchable": false,
                    //     "orderable": false,
                    //     "render": function (o) {
					// 		 if(o.assignbuilderid){
                    //             if(o.assignbuilderid.builder_name){

                    //                     return o.assignbuilderid.builder_name.name;
                    //             }
                    //             else{
                    //                 return '-';
                    //             }
                    //          }
                    //          else{
                    //              return '-';
                    //          }
					// 		//return o.assignbuilderid.builder_name.name;

                    //     }
			        // } ,
                    { data: 'job_number',name:'clients.job_number',"searchable" : true},
                     {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                           if(o.status == 'inactive')

                             status = "<a href='"+url+"/"+o.id+"?status=inactive' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('client.inactive')</button></a>";
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('client.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";

                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('client.view')</button></a>&nbsp;";


                            return v+e+d;
                        }
                    }

                ]
        });

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/clients/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete Client ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
// $(document).ready(function() {
//     $('#example').DataTable( {
//         "order": [[ 3, "desc" ]]
//     } );
// } );
</script>
@endpush
