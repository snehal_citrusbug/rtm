@extends('layouts.backend')

@section('title',trans('job.view_job_card'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('job.jobcard_list')</div>
                <div class="panel-body">

                    <a href="#" title="Back">
                        <button class="btn btn-warning btn-xs jobcard_back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('job.back')
                        </button>
                    </a>
                   
                    <br/>
                    <br/>
                    @foreach($jobcarddetail  as $jobcarddetails)
                        <label>Job Title</label>: {{$jobcarddetails->job_title}}<br/>
                        <label>Employee Name</label>: {{$jobcarddetails->jobcarduserName->name}}<br/>
                        <label>Second Client Name</label>:{{$jobcarddetails->second_client_name}}<br/>
                        <label>Total Price</label>: {{$jobcarddetails->total_amount}}<br/>
						
                    @endforeach

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                            </tr>
                             @foreach($jobcarddetail  as $jobcarddetails)
                                @foreach($jobcarddetails->jobcarddetailList as $cardlist)
                                    <tr>
                                            <td>{{$cardlist->job_card_name}}</td> 
                                            <td>{{$cardlist->job_card_qty}}</td>
                                            <td>{{$cardlist->job_card_price}}</td>
                                    </tr>
                                @endforeach
                         @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection