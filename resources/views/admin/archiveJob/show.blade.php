@extends('layouts.backend')

@section('title',trans('job.view_job'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('job.job')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('job.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Edit Job">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('job.edit_job')
                        </button>
                    </a>
                    @endif
                    
                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('job.id')</td>
                                <td>{{ $job->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('job.title')</td>
                                <td>{{ $job->title }}</td>
                            </tr>
                       
                            <tr>
                                <td>@lang('job.description')</td>
                                <td>{{ $job->description }}</td>
                            </tr>
                            
                            <tr>
                                <td>@lang('job.notes')</td>
                                <td>{{ $job->notes }}</td>
                            </tr>

                            <tr>
                                <td>@lang('job.duedate')</td>
                                <td>{{ $job->duedate }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection