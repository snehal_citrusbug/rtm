@extends('layouts.backend')


@section('title',trans('job.add_job'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('job.add_new_job')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('job.add_new_job')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('job.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/job', 'class' => 'form-horizontal','id'=>'formJob','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.job.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
