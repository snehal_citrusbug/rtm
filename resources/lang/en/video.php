<?php

return [

	'add_video' => 'Add Video',
    'add_new_video' => 'Add New Video',
    'back' => 'Back',
    'edit_video' => 'Edit Video',
    'update' => 'Update',
    'title' => 'Title',
	'videos' => 'Videos',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'video' => 'Video',
	'create' => 'Create',
	'video_url' => 'Video Url',
	'description' => 'Description',
	'addvideo'=>'Video',
	'changevideo'=>'Change Video',
	'view_video' => 'View Video',
];
