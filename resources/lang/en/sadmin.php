<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_user' => 'Add Admin',
    'add_new_user' => 'Add New Admin',
    'back' => 'Back',
    'edit_user' => 'Edit Admin',
    'update' => 'Update',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'role' => 'Role',
    'related_to' => 'Related To',
    'cpf' => 'CPF',
    'mobile' => 'Mobile',
    'phone' => 'Phone',
    'user_image' => 'Admin Image',
    'comments' => 'Comments',
    'create' => 'Create',
    'users' => 'Admin',
    'id' => 'ID',
    'profile' => 'Profile',
    'name' => 'Name',
    'email' => 'Email',
    'role' => 'Role',
    'company' => 'Company',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'view_user' => 'View Admin',
    'user' => 'Admin',
    'last_login_at' => 'Last Login At',



    'label' => [
        'cbusers' => 'ConnectBahn Admin',
        'id' => 'Id',
        'profile' => 'Profile',
        'name' => 'Name',
        'email' => 'Email',
        'role' => 'Role',
        'company' => 'Company',
        'status' => 'Status',
        'actions' => 'Actions',
        'goal' => 'Goal',
        'cbusertransaction' => 'Last 30d Transactions Amount',
        'assigncustorsupp' => 'Assigned Customers/Suppliers',
        'changegoal' => 'Change Goal',
        'changegoaluser' => 'Change Goal For User',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'blocked' => 'Blocked',
        'changeamount' => 'Change Goal Amount',
        'username' => 'User name'

        
    ],

    

];
