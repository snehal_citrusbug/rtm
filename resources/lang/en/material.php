<?php

return [

	'add_material' => 'Add Material',
    'add_new_material' => 'Add New Material',
    'back' => 'Back',
    'edit_material' => 'Edit Material',
    'update' => 'Update',
    'material_name' => ' Material Name',
	'materials' => 'Materials',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'create' => 'Create',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'view_material'=>'View Material',
	'material'=>'Material',
    'min_stock'=>'Min Stock',
	'stock'=>'Stock',
	'logs' => 'Logs',
	'user_name' => 'User Name',
    'addstock' => 'Add Stock',
    'total_quantity' => 'Total Quantity',
	'quantity' => 'Quantity',
	'added' => 'Added',
    'used' => 'Used',
    'description' => 'Description'
];