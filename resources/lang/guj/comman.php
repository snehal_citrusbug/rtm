<?php

return [


    'label'=>[
        'select_from_dropdown' => 'પસંદ કરો ..',
        'add_new' => 'નવો ઉમેરો',
        'mark' => 'ચિહ્ન',
        'action' => 'ક્રિયાઓ',
        'cancel' => 'રદ કરો',
        'save' => 'સાચવો',
        'update' => 'અપડેટ કરો',
        'submit' => 'સબમિટ કરો',
        'create' => 'બનાવો',
        'status' => 'સ્થિતિ',
        'edit' => 'સંપાદિત કરો',
        'back' => 'પાછળ',
        'delete' => 'કાઢી નાંખો',
        'created'=>'tithi',
        'select_subject' => "સંપાદિત subject",
        'select_site' => "સંપાદિત site",

    ],
    'datatable' =>[
        'search' => 'શોધો',
        'show' => 'બતાવો',
        'entries' => 'પ્રવેશો',
        'showing' => 'દર્શાવે',
        'to' => 'થી',
        'of' => 'ના',
        'small_entries' => 'પ્રવેશો',
        'paginate' => [
            'next' => 'આગળ',
            'previous' => 'અગાઉના',
            'first'=>'પ્રથમ',
            'last'=>'છેલ્લા',
        ],

    ],
    'responce_msg' =>[
        'something_went_wrong' => 'કંઈક ખોટું થયું. પછીથી ફરી પ્રયત્ન કરો.',
        'you_have_no_permision_to_delete_record' => 'તમારી પાસે આ રેકોર્ડને કાઢી નાખવાની પરવાનગી નથી',
        'record_deleted_succes' => 'રેકોર્ડ કાઢી સફળતા',
    ],
    'js_msg' =>[
        'confirm_for_delete' => 'શું તમે ખાતરી કરો કે :item_name કાઢી નાખો છો ?',
    ]

];
